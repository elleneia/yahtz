# yahtz

## Description

A simple 1-player console Yahtzee game.

The object of the game is to roll five dice in order to get the highest possible score.

## Usage

./yahtzee.py

## Requirements

Python3.7

## Rules

The player rolls five dice, and has the option to re-roll any or all of the dice two times. When the player is satisfied, or they have rolled a maximum of three times, they choose the most appropriate scoring rule and get awarded the corresponding points.

## Scoring

Aces Dice: Any combination Score: The sum of dice with the number 1

Twos Dice Any combination Score: The sum of dice with the number 2

Threes Dice Any combination Score: The sum of dice with the number 3

Fours Dice Any combination Score: The sum of dice with the number 4

Fives Dice Any combination Score: The sum of dice with the number 5

Sixes Dice Any combination Score: The sum of dice with the number 6

Three Of A Kind Dice: At least three dice the same Score: Sum of all dice

Four Of A Kind Dice: At least four dice the same Score: Sum of all dice

Full House Dice: Three of one number and two of another Score: 25

Small Straight Dice: Four sequential dice Score: (1-2-3-4, 2-3-4-5, or 3-4-5-6) 30

Large Straight Dice: Five sequential dice (1-2-3-4-5 or 2-3-4-5-6) Score: 40

Yahtzee Dice: All five dice the same Score: 50

FibonYahtzee Dice: All these five dice exactly: (1-1-2-3-5) Score: 100

Chance Dice: Any combination Score: Sum of all dice

## Roadmap

The game currently does not allow for "choosing to put a ZERO in any available spot" and have that turn count as one spent turn. The game concontinues to run, but the scoreboard does not indicate THIS ZERO should NOT be accessible for future turns. I made a couple attempts, but nothing that I tried worked.

I think it would be fun to add the additional "bonus points" on the top section, with a "carry down" of the top section subtotal (like the Yahtzee play cards.)

After making the changes on the single player mode, converting it to a multi-player web and/or mobile app would be a great exercise.

Part of the reasoning to refactor the code with this kind of design is the ability to reuse portions. I would like to try my hand at creating another dice game, utilizing the many of the portions of this code.

## Contributing

This is a learning project for me. I also have yet to work on an actual team project, start to finish, so my knowledge of how to manage a team is pretty sketchy.

I would like any contributors to have taken a look at the original code by Endemoniada, and look at Arjan's YouTube video of his analysis and refactoring of that code.

Ultimately, I think it would be great to have something to re-submit to Arjan for a code analysis. How well are we learning at applying these concepts?

## Authors and acknowledgment

This is the link to ArjanCodes refactoring video on YouTube:
https://youtu.be/l7E3y4te7sA
ArjanCodes
60,756 views May 21, 2021 Code Roasts
"""
Learn how to practically apply design patterns and principles by watching this full code refactoring of a Python project submitted by one of the subscribers. I start with a thorough analysis of the code, discuss its strengths and weaknesses, and then refactor the code to (hopefully :) ) improve it.
The code I worked on in this episode is available here:
https://github.com/ArjanCodes/coderoast-yahtzee
This repository contains both the original and adapted code for the Yahtzee Code Roast, a series where I refactor existing code projects with the aim to improve the design.
You can also find the original code here: https://github.com/Endemoniada/yahtzee.
"""

## License

GNU GENERAL PUBLIC LICENSE

## Project status

Until I get a little more experience or a contributor comes along who has more experience this project will NOT be in adcive development. My changes are minimal to Arjan's adapted code, so until more of the Roadmap is done, this project does not need a maintainer.
