import os
import re

from hand import Hand
from scoreboard import ScoreBoard
from yahtzee_rules import (Aces, Chance, FibonYahtzee, Fives, FourOfAKind,
                           Fours, FullHouse, LargeStraight, Sixes,
                           SmallStraight, ThreeOfAKind, Threes, Twos, Yahtzee)


class YahtzeeGame:
    def __init__(self):
        os.system('cls' if os.name == 'nt' else 'clear')
        print("""
            YAHTZEE

            Welcome to the game!
            Simply follow the instructions on the screen.

            Here is your initial scoreboard and your FIRST ROLL!!
            You will have 14 turns to get your highest score.
            You are 'on your honor' not to RESELECT an item after
              choosing to take a ZERO for that item's score.
            (And the game will crash if you try to Select a scoring
               item that is no longer zero. Working on it...)

            To exit, press [Ctrl+C]
            """)

        # Begin by instantiating the hand and scoreboard
        self.hand = Hand()
        self.scoreboard = ScoreBoard()

        # Register (define) the rules for Yahtzee
        self.scoreboard.register_rules([
            Aces(),
            Twos(),
            Threes(),
            Fours(),
            Fives(),
            Sixes(),
            ThreeOfAKind(),
            FourOfAKind(),
            FullHouse(),
            SmallStraight(),
            LargeStraight(),
            Yahtzee(),
            FibonYahtzee(),
            Chance(),
        ])

    def choose_dice_reroll(self):
        while True:
            try:
                reroll = input("\nChoose which dice to re-roll "
                               "(comma-separated or 'all'), or 0 to continue: ")
                '''
                potential input errors: 
                   mistype all, 
                   entering other than number, 
                   entering number greater than range of rules, 
                   listing too many die to reroll, 
                   entering choice(s) without comma-separation
                   '''

                # first test, "all", calling hand.all_dice(), rolling all dice
                if reroll.lower() == "all":
                    return self.hand.all_dice()
                else:
                    # Perform some clean-up of input
                    reroll = reroll.replace(" ", "")  # Take user input and replace spaces with no space
                    reroll = re.sub('[^0-9,]', '', reroll)  # Take that result and remove non-numerals
                    reroll = reroll.split(",")  # Take that result and turn string into list
                    reroll = list(map(int, reroll))  # Take that list of strings and turn strings in list to ints

                if not reroll or 0 in reroll:
                    return []
                else:
                    return reroll

            except ValueError:
                print("You entered something other than a number.")
                print("Please try again")
            except IndexError as i:
                print(i)

    # Need to translate this code from the original to the refactored method, in the above choose_dice_reroll() method:
    #            else:
    #                for i in reroll:
    #                    self.hand[i-1].roll()
    #                self.show_hand()
    #                rolls += 1

    def select_scoring(self):
        self.show_scoreboard_points(self.hand)
        while True:
            scoreboard_row = input("Choose which scoring to use (select from the *** items): ")
            print("Perform some clean-up of score selection input: Remove non-numerals")
            try:
                scoreboard_row_int = int(re.sub('[^0-9,]', '', scoreboard_row))
                print(f"scoreboard_row = {type(scoreboard_row)}{scoreboard_row}")
                print(f"execute the following: {int(re.sub('[^0-9,]', '', scoreboard_row))}")
                print(f"scoreboard_row_int: {type(scoreboard_row_int)}{scoreboard_row_int}")
                print(
                    f"IF {scoreboard_row_int}<0 or {scoreboard_row_int}>{self.scoreboard.rule_count()} \
                    then print('Please select an existing scoring rule.')")
                print(f"ELSE: {self.scoreboard.get_rule(scoreboard_row_int - 1)}")
                if scoreboard_row_int < 1 or scoreboard_row_int > self.scoreboard.rule_count():
                    print("Please select an existing scoring rule.")
                else:
                    return self.scoreboard.get_rule(scoreboard_row_int - 1)
            except ValueError:
                print("You entered something other than a number. Please try again.")

    def show_scoreboard_points(self, hand: Hand = None):
        print("\nSCOREBOARD")
        print("===================================")
        print(self.scoreboard.create_points_overview(hand))
        print("===================================")

    def do_turn(self):
        rolls = 0
        selected_dice = self.hand.all_dice()
        while True:
            # roll the dice
            print("\nYour Current ScoreCard")
            print("===================================")
            print(self.scoreboard.create_points_overview())
            print("===================================")
            print("\nRolling dice...")
            self.hand.roll(selected_dice)
            print(self.hand)
            rolls += 1

            # If we reach the maximum number of rolls, we're done.
            if rolls >= 3:
                break

            # Choose which dice to reroll, break if empty
            selected_dice = self.choose_dice_reroll()
            if len(selected_dice) == 0:
                break

        rule = self.select_scoring()

        points = self.scoreboard.assign_points(rule, self.hand)
        '''
        Call of 'scoreboard.assign_points()' needs: 
           Try,Except to solve CRASH when exception is raised: "Exception("ScoreBoard already saved!")" 
           Also need a way to PREVENT user from RE-SELECTING a previous turn's ZERO points choice.'''
        print(f"Adding {points} points to {rule.name()}")
        self.show_scoreboard_points()

        input("\nPress any key to continue")
        os.system('cls' if os.name == 'nt' else 'clear')

    def play(self):

        # We keep going until the scoreboard is full
        for _ in range(self.scoreboard.rule_count()):
            self.do_turn()

        print("\nCongratulations! You finished the game!\n")
        self.show_scoreboard_points()
        print(f"Total points: {self.scoreboard.total_points()}")


if __name__ == '__main__':
    try:
        game = YahtzeeGame()
        game.play()
    except KeyboardInterrupt:
        print("\nExiting...")
